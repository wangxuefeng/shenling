package com.sl.gateway.filter;

import cn.hutool.core.collection.CollUtil;
import com.itheima.auth.sdk.AuthTemplate;
import com.itheima.auth.sdk.common.Result;
import com.itheima.auth.sdk.dto.AuthUserInfoDTO;
import com.itheima.auth.sdk.service.TokenCheckService;
import com.sl.gateway.config.MyConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;

@Component
public class ManagerTokenGatewayFilterFactory extends AbstractGatewayFilterFactory<Object> implements AuthFilter {

    @Resource
    private MyConfig myConfig;
    @Resource
    private TokenCheckService tokenCheckService;
    @Resource
    private AuthTemplate authTemplate;
    @Value("${role.manager}")
    private List<Long> ramagerRoleList;

    @Override
    public GatewayFilter apply(Object config) {
        return new TokenGatewayFilter(this, myConfig);
    }

    @Override
    public AuthUserInfoDTO check(String token) {
        try {
            return this.tokenCheckService.parserToken(token);
        } catch (Exception e) {
            //忽略异常，不打印
        }
        return null;
    }

    @Override
    public Boolean auth(String token, AuthUserInfoDTO authUserInfo, String path) {
        authTemplate.getAuthorityConfig().setToken(token);
        Result<List<Long>> roleResult = authTemplate.opsForRole().findRoleByUserId(authUserInfo.getUserId());
        List<Long> roleList = roleResult.getData(); //当前用户拥有的角色
        Collection<Long> intersection = CollUtil.intersection(this.ramagerRoleList, roleList);
        return CollUtil.isNotEmpty(intersection);
    }
}
